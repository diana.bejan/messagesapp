package com.example.messages.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Messages.db";
    private static final String CREATE_TABLE_SQL_QUERY = "CREATE TABLE " + Schema.Messages.TABLE_NAME + "(" +
            Schema.Messages._ID + " INTEGER PRIMARY KEY, " + Schema.Messages.COLUMN_NAME_CONTENT +
            " TEXT, " + Schema.Messages.COLUMN_NAME_PHONE_NUMBER + " LONG, " + Schema.Messages.COLUMN_NAME_TIMESTAMP +
            " LONG, " + Schema.Messages.COLUMN_NAME_DIRECTION + " INT);";

    public DBHelper(@Nullable final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_SQL_QUERY);
        } catch (final SQLException e) {
            System.out.printf("Exception: Table may be already created: %s\n", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // NOP
    }
}
