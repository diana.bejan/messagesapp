package com.example.messages.models;

import java.text.SimpleDateFormat;

public class ConversationPreview implements Comparable<ConversationPreview> {
    private String phoneNumber;
    private String messagePreview;
    private long timestamp;

    public ConversationPreview(final String phoneNumber, final String messagePreview, final long timestamp) {
        this.phoneNumber = phoneNumber;
        this.messagePreview = messagePreview;
        this.timestamp = timestamp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessagePreview() {
        return messagePreview;
    }

    public void setMessagePreview(final String messagePreview) {
        this.messagePreview = messagePreview;
    }

    public String getTimestamp() {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss ");
        return formatter.format(this.timestamp);
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        if (messagePreview.length() < 23) {
            return messagePreview;
        } else {
            return messagePreview.substring(0, 20) + "...";
        }
    }


    @Override
    public int compareTo(final ConversationPreview c) {
        if (this.timestamp > c.timestamp)
            return -1;
        return 1;
    }
}
