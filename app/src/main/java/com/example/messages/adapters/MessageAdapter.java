package com.example.messages.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.messages.R;
import com.example.messages.models.Message;


import java.util.List;

import androidx.annotation.NonNull;

public class MessageAdapter extends ArrayAdapter<Message> {


    public MessageAdapter(@NonNull final Context context, @NonNull final List<Message> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final Message item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_conversation_message, parent, false);
        }

        final TextView messageView = (TextView) convertView.findViewById(R.id.text_conversation_message);
        messageView.setText(item.getContent());
        final TextView dateView = (TextView) convertView.findViewById(R.id.text_conversation_time);
        dateView.setText(item.getFormatedTimestamp());
        if (item.getDirection() == Message.SMS_TYPE_INCOMING) {
            convertView.setBackground(convertView.getResources().getDrawable(R.drawable.message_incoming));
            convertView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

        } else {
            convertView.setBackground(convertView.getResources().getDrawable(R.drawable.message_outgoing));
            convertView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
        }

        return convertView;
    }
}