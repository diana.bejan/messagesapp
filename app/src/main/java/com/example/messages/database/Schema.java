package com.example.messages.database;

import android.provider.BaseColumns;

public class Schema {

    private Schema() {
    }

    public static class Messages implements BaseColumns {
        public static final String TABLE_NAME = "messages";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_DIRECTION = "direction";
        public static final String COLUMN_NAME_PHONE_NUMBER = "phoneNumber";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }
}
