package com.example.messages.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.messages.models.ConversationPreview;
import com.example.messages.models.Message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DB {
    private DBHelper dbHerper;
    private static final String SQL_GET_LAST_MESSAGES = "SELECT * FROM " + Schema.Messages.TABLE_NAME
            + " s1 WHERE " + Schema.Messages.COLUMN_NAME_TIMESTAMP + " = (SELECT MAX(" + Schema.Messages.COLUMN_NAME_TIMESTAMP
            + ") FROM " + Schema.Messages.TABLE_NAME + " s2 WHERE s2." + Schema.Messages.COLUMN_NAME_PHONE_NUMBER
            + " = s1." + Schema.Messages.COLUMN_NAME_PHONE_NUMBER + ")";


    public DB(final Context context) {
        this.dbHerper = new DBHelper(context);
    }

    public long insert(final Message m) {
        final SQLiteDatabase database = this.dbHerper.getWritableDatabase();

        final ContentValues values = new ContentValues();
        values.put(Schema.Messages.COLUMN_NAME_CONTENT, m.getContent());
        values.put(Schema.Messages.COLUMN_NAME_DIRECTION, m.getDirection());
        values.put(Schema.Messages.COLUMN_NAME_PHONE_NUMBER, m.getPhoneNumber());
        values.put(Schema.Messages.COLUMN_NAME_TIMESTAMP, m.getTimestamp());
        final long iid = database.insert(Schema.Messages.TABLE_NAME, null, values);
        System.out.println(iid);
        return iid;
    }

    public List<Message> getConversation(final String number) {
        final SQLiteDatabase database = this.dbHerper.getReadableDatabase();
        final String[] columns = new String[]{Schema.Messages.COLUMN_NAME_DIRECTION,
                Schema.Messages.COLUMN_NAME_TIMESTAMP,
                Schema.Messages.COLUMN_NAME_PHONE_NUMBER,
                Schema.Messages.COLUMN_NAME_CONTENT};
        final Cursor cursor = database.query(
                Schema.Messages.TABLE_NAME,
                columns,
                Schema.Messages.COLUMN_NAME_PHONE_NUMBER + " =? ",
                new String[]{number},
                null,
                null,
                Schema.Messages.COLUMN_NAME_TIMESTAMP);
        cursor.moveToFirst();
        return decodeMessages(cursor);
    }

    public List<ConversationPreview> getPreviews() {
        final SQLiteDatabase db = dbHerper.getReadableDatabase();
        final Cursor cursor = db.rawQuery(SQL_GET_LAST_MESSAGES, null);

        final List<ConversationPreview> messages = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                messages.add(new ConversationPreview(
                        cursor.getString(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_PHONE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_CONTENT)),
                        cursor.getLong(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_TIMESTAMP))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        Collections.sort(messages);
        System.out.println(messages);
        return messages;
    }

    private List<Message> decodeMessages(final Cursor cursor) {
        final List<Message> messages = new ArrayList<>();
        while (cursor.moveToNext()) {
            final String content = cursor.getString(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_CONTENT));
            final String number = cursor.getString(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_PHONE_NUMBER));
            final int direction = cursor.getInt(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_DIRECTION));
            final long timestamp = cursor.getLong(cursor.getColumnIndex(Schema.Messages.COLUMN_NAME_TIMESTAMP));
            final Message message = new Message(number, content, direction, timestamp);
            messages.add(message);
        }
        System.out.println(messages);
        Collections.sort(messages);
        System.out.println();
        System.out.println(messages);
        System.out.println();

        return messages;
    }
}
