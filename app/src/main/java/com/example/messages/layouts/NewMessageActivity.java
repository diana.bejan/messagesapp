package com.example.messages.layouts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.messages.R;
import com.example.messages.helpers.MessageSender;

public class NewMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
    }

    public void sendMessageTo(final View view) {
        final EditText contentView = (EditText) findViewById(R.id.text_view_message_content_new);
        final EditText phoneView = (EditText) findViewById(R.id.text_view_message_destination_new);
        MessageSender.sendSMS(this, phoneView.getText().toString(), contentView.getText().toString());
        contentView.setText("");
    }
}
