package com.example.messages.helpers;

import android.content.Context;
import android.telephony.SmsManager;

import com.example.messages.database.DB;
import com.example.messages.models.Message;

public class MessageSender {

    private MessageSender() {
    }

    public static boolean sendSMS(final Context ctx, final String to, final String message) {


        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(to, null, message, null, null);

        final DB db = new DB(ctx);
        db.insert(new Message(to, message, Message.SMS_TYPE_OUTGOING, System.currentTimeMillis()));

        return true;
    }
}
