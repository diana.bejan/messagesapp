package com.example.messages.models;


import java.text.SimpleDateFormat;

public class Message implements Comparable<Message> {

    public static final int SMS_TYPE_INCOMING = 0;
    public static final int SMS_TYPE_OUTGOING = 1;

    private String phoneNumber;
    private String content;
    private int direction;
    private long timestamp;

    public Message(final String phoneNumber, final String content, final int direction, final long timestamp) {
        this.phoneNumber = phoneNumber;
        this.content = content;
        this.direction = direction;
        this.timestamp = timestamp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(final int direction) {
        this.direction = direction;
    }

    public String getFormatedTimestamp() {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss ");
        return formatter.format(this.timestamp);
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(final Message m) {
        if (this.timestamp > m.timestamp)
            return 1;
        return -1;
    }

    @Override
    public String toString() {
        if (content.length() < 23) {
            return timestamp + ": " + content;
        } else {
            return timestamp + ": " + content.substring(0, 20) + "...";
        }
    }
}
