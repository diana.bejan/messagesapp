package com.example.messages.layouts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.example.messages.R;
import com.example.messages.adapters.ConversationPreviewAdapter;
import com.example.messages.adapters.MessageAdapter;
import com.example.messages.database.DB;
import com.example.messages.helpers.Extra;
import com.example.messages.helpers.MessageSender;
import com.example.messages.models.ConversationPreview;
import com.example.messages.models.Message;

import java.util.List;

public class ConversationActivity extends AppCompatActivity {

    private static ConversationActivity activeInstance = null;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation_layout);
        this.phone = getIntent().getStringExtra(Extra.KEY_EXTRA_PHONE);
        buildList();
        activeInstance = this;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void sendMessage(final View view) {
        final EditText contentView = (EditText) findViewById(R.id.text_view_message_content_conversation);
        MessageSender.sendSMS(this, phone, contentView.getText().toString());
        contentView.setText("");
        refreshIfActive();
    }

    @Override
    protected void onPause() {
        super.onPause();

        activeInstance = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        activeInstance = this;
        buildList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void refreshIfActive() {
        if (activeInstance != null)
            activeInstance.buildList();
    }

    private void buildList() {
        final DB db = new DB(this);
        final List<Message> messages = db.getConversation(this.phone);
        final MessageAdapter messageAdapter =
                new MessageAdapter(this, messages);
        final ListView listView = (ListView) findViewById(R.id.list_messages_conversation);
        listView.setAdapter(messageAdapter);
    }
}
