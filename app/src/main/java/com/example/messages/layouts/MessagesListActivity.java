package com.example.messages.layouts;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.messages.R;
import com.example.messages.adapters.ConversationPreviewAdapter;
import com.example.messages.database.DB;
import com.example.messages.helpers.Extra;
import com.example.messages.models.ConversationPreview;

import java.util.List;

public class MessagesListActivity extends AppCompatActivity {

    private List<ConversationPreview> previews;
    private static MessagesListActivity activeInstance;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_list);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        buildList();
        activeInstance = this;
    }

    private void buildList() {
        final DB db = new DB(this);
        this.previews = db.getPreviews();

        final ConversationPreviewAdapter conversationPreviewAdapter =
                new ConversationPreviewAdapter(this, this.previews);
        final ListView listView = (ListView) findViewById(R.id.list_conversations_headers);
        listView.setAdapter(conversationPreviewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                final Intent intent = new Intent(MessagesListActivity.this,
                        ConversationActivity.class);
                intent.putExtra(Extra.KEY_EXTRA_PHONE, previews.get(position).getPhoneNumber());
                startActivity(intent);
            }
        });
    }

    public void openNewMessageActivity(final View view) {
        final Intent intent = new Intent(this, NewMessageActivity.class);
        this.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();

        activeInstance = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        activeInstance = this;
        buildList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void refreshIfActive() {
        if (activeInstance != null)
            activeInstance.buildList();
    }

}
