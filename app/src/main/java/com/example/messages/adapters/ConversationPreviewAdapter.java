package com.example.messages.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.messages.R;
import com.example.messages.models.ConversationPreview;

import java.util.List;

import androidx.annotation.NonNull;

public class ConversationPreviewAdapter extends ArrayAdapter<ConversationPreview> {

    public ConversationPreviewAdapter(@NonNull final Context context, @NonNull final List<ConversationPreview> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ConversationPreview item = getItem(position);


        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_conversation_header, parent, false);
        }

        final TextView messageView = (TextView) convertView.findViewById(R.id.text_conversation_header_message);
        messageView.setText(item.toString());
        final TextView dateView = (TextView) convertView.findViewById(R.id.text_conversation_header_date);
        dateView.setText(item.getTimestamp());
        final TextView phoneView = (TextView) convertView.findViewById(R.id.text_conversation_header_number);
        phoneView.setText(item.getPhoneNumber());

        return convertView;
    }
}
