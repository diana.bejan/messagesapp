package com.example.messages.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.example.messages.database.DB;
import com.example.messages.layouts.ConversationActivity;
import com.example.messages.layouts.MessagesListActivity;
import com.example.messages.models.Message;

public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            final SmsMessage[] smss = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            final DB db = new DB(context);
            for (SmsMessage sms : smss) {
                db.insert(
                        new Message(
                                sms.getOriginatingAddress(),
                                sms.getMessageBody(),
                                Message.SMS_TYPE_INCOMING,
                                System.currentTimeMillis()
                        )
                );
            }
            MessagesListActivity.refreshIfActive();
            ConversationActivity.refreshIfActive();
        }
    }

}
